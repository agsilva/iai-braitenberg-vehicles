﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class LightDetectorScript : MonoBehaviour {

	public float angle;

	public float output;
	public int numObjects;

	private Light spotlight;
	public bool allLights; //All lights or just visible lights 

	//Bounded variables
	public float boundedMin = 0;
	public float boundedMax = 1;

	//Threshold variables
	public float thresholdMin = 0;
	public float thresholdMax = 1;

	public List activationFunction; //Linear or Gaussian

	public float gaussMean;
	public float gaussStdDev;
	public float gaussBias;

	void Start () {
		output = 0;
		numObjects = 0;
		spotlight = GetComponentInChildren<Light> ();
		spotlight.spotAngle = angle;
	}

	void Update () {
		
		GameObject[] lights;
		output = 0;

		if (allLights)
			lights = GetAllLights ();
		else
			lights = GetVisibleLights ();
		
		numObjects = lights.Length;

		foreach (GameObject light in lights) {
			float r = light.GetComponent<Light> ().range;
			output += 1f / Mathf.Pow ((transform.position - light.transform.position).magnitude / r + 1, 2);
		}

		if (numObjects > 0)
			output = output / numObjects;
		
	}
		
	public float getOutput(){
		Debug.Log ("Lights Output: "+ activationFunction);

		if (activationFunction.ToString ().Equals ("Linear"))
			return getLinearOutput ();

		return getGaussOutput ();

	}

	// Get Sensor Linear output value
	public float getLinearOutput(){

		return getThresholdOutput();
	}

	// Get Sensor Gaussian output value
	public float getGaussOutput(){

		return gaussMean * Mathf.Pow (2.71828f, (-Mathf.Pow (getThresholdOutput() - gaussStdDev, 2)) / (2 * Mathf.Pow (gaussBias,2)));
	}

	// Get Sensor Threshold output value
	public float getThresholdOutput(){

		if (output < thresholdMin || output > thresholdMax)
			return 0;
		else
			return getBoundedOutput();
	}

	// Get Sensor Bounded output value
	public float getBoundedOutput(){

		return Mathf.Max (boundedMin, Mathf.Min (boundedMax, output));
	}

	// Returns all "Light" tagged objects. The sensor angle is not taken into account.
	GameObject[] GetAllLights()
	{
		return GameObject.FindGameObjectsWithTag ("Light");
	}

	// Returns all "Light" tagged objects that are within the view angle of the Sensor. Only considers the angle over 
	// the y axis. Does not consider objects blocking the view.
	GameObject[] GetVisibleLights()
	{
		ArrayList visibleLights = new ArrayList();
		float halfAngle = angle / 2.0f;

		GameObject[] lights = GameObject.FindGameObjectsWithTag ("Light");

		foreach (GameObject light in lights) {
			Vector3 toVector = (light.transform.position - transform.position);
			Vector3 forward = transform.forward;
			toVector.y = 0;
			forward.y = 0;
			float angleToTarget = Vector3.Angle (forward, toVector);

			if (angleToTarget <= halfAngle) {
				visibleLights.Add (light);
				Debug.Log ("Vi a luz " + light.name);
			}
		}

		return (GameObject[])visibleLights.ToArray(typeof(GameObject));
	}	

	public enum List {Linear, Gaussian}
}
	
