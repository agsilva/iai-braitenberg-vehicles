﻿using UnityEngine;
using System.Collections;

public class SmartAgressiveBehaviour : CarBehaviour {

	public BlockDetectorScript LeftBD;
	public BlockDetectorScript RightBD;

	void Update()
	{
		float leftSensor;
		float rightSensor; 
		float leftBSensor;
		float rightBSensor;

		//Read light sensor values
		leftSensor = LeftLD.getOutput();
		rightSensor = RightLD.getOutput();

		//Read block sensor values
		leftBSensor = LeftBD.getOutput ();
		rightBSensor = RightBD.getOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = (leftSensor + leftBSensor) / 2 * MaxSpeed;
		m_RightWheelSpeed = (rightSensor + rightBSensor) / 2 * MaxSpeed;


		//Calculate target motor values
		m_LeftWheelSpeed = (rightSensor + leftBSensor) / 2 * MaxSpeed;
		m_RightWheelSpeed = (leftSensor + rightBSensor) / 2 * MaxSpeed;
	}
}
