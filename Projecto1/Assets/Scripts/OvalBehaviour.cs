﻿using UnityEngine;
using System.Collections;

public class OvalBehaviour : CarBehaviour {

	private float leftSensor;
	private float rightSensor;


	void Update()
	{
		//Read sensor values
//		leftSensor = LeftLD.getGaussOutput(0.85f,0f,1.11f);
//		rightSensor = RightLD.getGaussOutput(1.103f,0.711f,1.05f);
		leftSensor = LeftLD.getOutput();
		rightSensor = RightLD.getOutput();

		Debug.Log ("Left = " + leftSensor);
		Debug.Log ("Right = " + rightSensor);
		Debug.Log ("x");

		//Calculate target motor values
		m_LeftWheelSpeed = leftSensor * MaxSpeed;
		m_RightWheelSpeed = rightSensor * MaxSpeed;
	}
}
