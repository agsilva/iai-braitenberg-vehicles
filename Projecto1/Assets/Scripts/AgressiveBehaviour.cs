﻿using UnityEngine;
using System.Collections;

public class AgressiveBehaviour : CarBehaviour {

	void Update()
	{
		float leftSensor;
		float rightSensor;

		//Read sensor values
		leftSensor = LeftLD.getOutput ();
		rightSensor = RightLD.getOutput ();
			
		//Calculate target motor values
		m_LeftWheelSpeed = rightSensor * MaxSpeed;
		m_RightWheelSpeed = leftSensor * MaxSpeed;
	}
}
