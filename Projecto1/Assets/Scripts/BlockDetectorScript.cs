﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class BlockDetectorScript : MonoBehaviour {

	public float angle;

	public float output;
	public int numObjects;
	private float currOutput;

	//Bounded variables
	public float boundedMin = 0;
	public float boundedMax = 1;

	//Threshold variables
	public float thresholdMin = 0;
	public float thresholdMax = 1;

	public List activationFunction; //Linear or Gaussian

	public float gaussMean;
	public float gaussStdDev;
	public float gaussBias;

	void Start () {
		output = 0;
		numObjects = 0;
	}

	void Update () {
		GameObject[] cubes = GetVisibleCubes ();

		output = 0;
		numObjects = cubes.Length;

		if (numObjects > 0){				
			foreach (GameObject cube in cubes) {
				currOutput = 1f / ((transform.position - cube.transform.position).magnitude + 1);

				if (currOutput > output)
					output = currOutput;
			}
		}
	}

	public float getOutput(){
		Debug.Log ("Blocks Output: "+ activationFunction);

		if (activationFunction.ToString ().Equals ("Linear"))
			return getLinearOutput ();

		return getGaussOutput ();

	}

	// Get Sensor Linear output value
	public float getLinearOutput(){

		return getThresholdOutput();
	}

	// Get Sensor Gaussian output value
	public float getGaussOutput(){

		return gaussMean * Mathf.Pow (2.71828f, (-Mathf.Pow (getThresholdOutput() - gaussStdDev, 2)) / (2 * Mathf.Pow (gaussBias,2)));
	}

	// Get Sensor Threshold output value
	public float getThresholdOutput(){

		if (output < thresholdMin || output > thresholdMax)
			return 0;
		else
			return getBoundedOutput();
	}

	// Get Sensor Bounded output value
	public float getBoundedOutput(){

		return Mathf.Max (boundedMin, Mathf.Min (boundedMax, output));
	}
		
	// Returns all "Cube" tagged objects that are within the view angle of the Sensor. Only considers the angle over 
	// the y axis. Does not consider objects blocking the view.
	GameObject[] GetVisibleCubes()
	{
		ArrayList visibleCubes = new ArrayList();
		float halfAngle = angle / 2.0f;

		GameObject[] cubes = GameObject.FindGameObjectsWithTag ("Cube");

		foreach (GameObject cube in cubes) {
			Vector3 toVector = (cube.transform.position - transform.position);
			Vector3 forward = transform.forward;
			toVector.y = 0;
			forward.y = 0;
			float angleToTarget = Vector3.Angle (forward, toVector);

			if (angleToTarget <= halfAngle) {
				visibleCubes.Add (cube);
			}
		}

		return (GameObject[])visibleCubes.ToArray(typeof(GameObject));
	}

	public enum List {Linear, Gaussian} 
}