﻿using UnityEngine;
using System.Collections;

public class EightieBehaviour : CarBehaviour {

	public LightDetectorScript LeftExtraLD;
	public LightDetectorScript RightExtraLD;

	void Update()
	{
		//Read sensor values
//		float leftSensor = LeftLD.getBoundedOutput(0.6f,0.5f);
//		float rightSensor = RightLD.getBoundedOutput(0.6f,0.5f);
//		float leftextraSensor = LeftExtraLD.getBoundedOutput(0.097f,0.00f);
//		float rightextraSensor = RightExtraLD.getBoundedOutput(0.097f,0.00f);
		float leftSensor = LeftLD.getOutput();
		float rightSensor = RightLD.getOutput();
		float leftextraSensor = LeftExtraLD.getOutput();
		float rightextraSensor = RightExtraLD.getOutput();


//		Debug.Log ("Left = " + (leftSensor+leftextraSensor));
//		Debug.Log ("Right = " + (rightSensor+rightextraSensor));
//		Debug.Log ("x");

		//Calculate target motor values
		m_LeftWheelSpeed = (leftSensor+rightextraSensor ) * MaxSpeed;
		m_RightWheelSpeed =  (rightSensor+leftextraSensor) * MaxSpeed;
	}
}
