﻿using UnityEngine;
using System.Collections;

public class CowardBehaviour : CarBehaviour {
	
	void Update()
	{
		float leftSensor;
		float rightSensor;

		//Read sensor values
		leftSensor = LeftLD.getOutput ();
		rightSensor = RightLD.getOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = leftSensor * MaxSpeed;
		m_RightWheelSpeed = rightSensor * MaxSpeed;
	}
}
